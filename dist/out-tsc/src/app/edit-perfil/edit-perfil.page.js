var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UsuarioService } from '../usuario.service';
import { HttpClient } from '@angular/common/http';
var EditPerfilPage = /** @class */ (function () {
    function EditPerfilPage(navCtrl, servidor, http) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.servidor = servidor;
        this.http = http;
        this.url = "http://localhost/php/";
        this.http.get(this.url + 'dados_usuario.php').subscribe(function (res) {
            _this.contatos = res;
            console.log(_this.contatos);
        });
        this.getRetornar();
    }
    EditPerfilPage.prototype.ngOnInit = function () {
    };
    EditPerfilPage.prototype.getRetornar = function () {
        var _this = this;
        this.servidor.getPegar()
            .subscribe(function (data) { return _this.contatos = data; }, function (err) { return console.log(err); });
    };
    EditPerfilPage = __decorate([
        Component({
            selector: 'app-edit-perfil',
            templateUrl: './edit-perfil.page.html',
            styleUrls: ['./edit-perfil.page.scss'],
        }),
        __metadata("design:paramtypes", [NavController, UsuarioService, HttpClient])
    ], EditPerfilPage);
    return EditPerfilPage;
}());
export { EditPerfilPage };
//# sourceMappingURL=edit-perfil.page.js.map