var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
var routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
    { path: 'tab4', loadChildren: './tab4/tab4.module#Tab4PageModule' },
    { path: 'dica-alongamento', loadChildren: './dica-alongamento/dica-alongamento.module#DicaAlongamentoPageModule' },
    { path: 'dica-treino', loadChildren: './dica-treino/dica-treino.module#DicaTreinoPageModule' },
    { path: 'dica-musculacao', loadChildren: './dica-musculacao/dica-musculacao.module#DicaMusculacaoPageModule' },
    { path: 'dica-suplemento', loadChildren: './dica-suplemento/dica-suplemento.module#DicaSuplementoPageModule' },
    { path: 'agenda', loadChildren: './agenda/agenda.module#AgendaPageModule' },
    { path: 'competicao', loadChildren: './competicao/competicao.module#CompeticaoPageModule' },
    { path: 'agenda', loadChildren: './agenda/agenda.module#AgendaPageModule' },
    { path: 'edit-perfil', loadChildren: './edit-perfil/edit-perfil.module#EditPerfilPageModule' },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'cadastro', loadChildren: './cadastro/cadastro.module#CadastroPageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map