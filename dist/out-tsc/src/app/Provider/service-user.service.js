var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
var ServiceUserService = /** @class */ (function () {
    function ServiceUserService() {
        this.userProva = "";
        this.userTempo = "";
        this.userMeta = "";
        this.userCategoria = "";
        this.userNome = "";
        this.idUser = "";
        this.userNivel = "";
        this.userFoto = "";
    }
    ServiceUserService.prototype.setUserNome = function (valor) {
        this.userNome = valor;
    };
    ServiceUserService.prototype.getUserNome = function () {
        return localStorage.getItem("userNome");
    };
    ServiceUserService.prototype.setUserCategoria = function (valor) {
        this.userCategoria = valor;
    };
    ServiceUserService.prototype.getUserCategoria = function () {
        return localStorage.getItem("userCategoria");
    };
    ServiceUserService.prototype.setUserId = function (valor) {
        this.idUser = valor;
    };
    ServiceUserService.prototype.getUserId = function () {
        return this.idUser;
    };
    ServiceUserService.prototype.setUserNivel = function (valor) {
        this.userNivel = valor;
    };
    ServiceUserService.prototype.getUserNivel = function () {
        return this.userNivel;
    };
    ServiceUserService.prototype.setUserFoto = function (valor) {
        this.userFoto = valor;
    };
    ServiceUserService.prototype.getUserFoto = function () {
        return this.userFoto;
    };
    ServiceUserService.prototype.setUserProva = function (valor) {
        this.userProva = valor;
    };
    ServiceUserService.prototype.getUserProva = function () {
        return localStorage.getItem("userProva");
    };
    ServiceUserService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], ServiceUserService);
    return ServiceUserService;
}());
export { ServiceUserService };
//# sourceMappingURL=service-user.service.js.map