var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { UsuarioService } from '../usuario.service';
var CompeticaoPage = /** @class */ (function () {
    function CompeticaoPage(servidor) {
        this.servidor = servidor;
        this.getRetornar();
    }
    CompeticaoPage.prototype.ngOnInit = function () {
    };
    CompeticaoPage.prototype.getRetornar = function () {
        var _this = this;
        this.servidor.getCompeticao()
            .subscribe(function (data) { return _this.dados = data; }, function (err) { return console.log(err); });
        this.servidor.getCidade()
            .subscribe(function (data) { return _this.dadoItem = data; }, function (err) { return console.log(err); });
    };
    CompeticaoPage = __decorate([
        Component({
            selector: 'app-competicao',
            templateUrl: './competicao.page.html',
            styleUrls: ['./competicao.page.scss'],
        }),
        __metadata("design:paramtypes", [UsuarioService])
    ], CompeticaoPage);
    return CompeticaoPage;
}());
export { CompeticaoPage };
//# sourceMappingURL=competicao.page.js.map