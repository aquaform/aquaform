var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Headers } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../Provider/url.service";
import { NavController } from '@ionic/angular';
var Tab3Page = /** @class */ (function () {
    function Tab3Page(navCtrl, formBuilder, http, urlService) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.http = http;
        this.urlService = urlService;
        this.postagem = this.formBuilder.group({
            inicio: ['', Validators.required],
            fim: ['', Validators.required],
            horario: ['', Validators.required],
            tipo: ['', Validators.required],
            descricao: ['', Validators.required]
        });
    }
    Tab3Page.prototype.postarProduto = function () {
        if (this.inicio == undefined || this.fim == undefined || this.horario == undefined || this.tipo == undefined || this.descricao == undefined) {
            this.urlService.alertas('Atenção', 'Preencha todos os campos!');
        }
        else {
            this.postData(this.postagem.value)
                .subscribe(function (data) {
                console.log("Inserido com sucesso!!!");
            }, function (err) {
                console.log("Erro ao tentar inserir!!!");
            });
        }
    };
    Tab3Page.prototype.postData = function (tipo) {
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.http.post(this.urlService.url + "insertEvento.php", tipo, {
            headers: headers,
            method: "POST"
        }).pipe(map(function (res) { return res.json(); }));
    };
    Tab3Page.prototype.ngOnInit = function () {
    };
    Tab3Page = __decorate([
        Component({
            selector: 'app-tab3',
            templateUrl: 'tab3.page.html',
            styleUrls: ['tab3.page.scss']
        }),
        __metadata("design:paramtypes", [NavController,
            FormBuilder,
            Http,
            UrlService])
    ], Tab3Page);
    return Tab3Page;
}());
export { Tab3Page };
//# sourceMappingURL=tab3.page.js.map