var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import chartJs from 'chart.js';
import { UsuarioService } from '../usuario.service';
var Tab1Page = /** @class */ (function () {
    function Tab1Page(navCtrl, servidor) {
        this.navCtrl = navCtrl;
        this.servidor = servidor;
        this.getRetornar();
    }
    Tab1Page.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.barChart = _this.getBarChart();
        }, 150);
    };
    Tab1Page.prototype.getChart = function (context, chartType, data, options) {
        return new chartJs(context, {
            data: data,
            options: options,
            type: chartType
        });
    };
    Tab1Page.prototype.getBarChart = function () {
        var data = {
            labels: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
            datasets: [{
                    label: 'Compromissos da semana',
                    data: [2, 1, 5, 1, 1, 2, 3],
                    backgroundColor: [
                        'rgb(56, 128, 255)',
                        'rgb(56, 128, 255)',
                        'rgb(56, 128, 255)',
                        'rgb(56, 128, 255)',
                        'rgb(56, 128, 255)',
                        'rgb(56, 128, 255)',
                        'rgb(56, 128, 255)'
                    ],
                    borderWidth: 1
                }]
        };
        var options = {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
            }
        };
        return this.getChart(this.barCanvas.nativeElement, 'bar', data, options);
    };
    Tab1Page.prototype.getRetornar = function () {
        var _this = this;
        this.servidor.getCompeticao()
            .subscribe(function (data) { return _this.dados = data; }, function (err) { return console.log(err); });
    };
    __decorate([
        ViewChild('barCanvas'),
        __metadata("design:type", Object)
    ], Tab1Page.prototype, "barCanvas", void 0);
    Tab1Page = __decorate([
        Component({
            selector: 'app-tab1',
            templateUrl: 'tab1.page.html',
            styleUrls: ['tab1.page.scss']
        }),
        __metadata("design:paramtypes", [NavController, UsuarioService])
    ], Tab1Page);
    return Tab1Page;
}());
export { Tab1Page };
//# sourceMappingURL=tab1.page.js.map