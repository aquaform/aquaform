var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Headers } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../Provider/url.service";
import { NavController } from '@ionic/angular';
import { UsuarioService } from '../usuario.service';
var CadastroPage = /** @class */ (function () {
    function CadastroPage(navCtrl, formBuilder, Http, urlService, servidor, navController) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.Http = Http;
        this.urlService = urlService;
        this.servidor = servidor;
        this.navController = navController;
        this.postagem = this.formBuilder.group({
            nome: ['', Validators.required],
            categoria: ['', Validators.required],
            //ano: ['', Validators.required],
            email: ['', Validators.required],
            senha: ['', Validators.required],
            prova: ['', Validators.required]
        });
        this.getRetornar();
        this.informacoes();
        this.informacoesCategoria();
    }
    CadastroPage.prototype.postarProduto = function () {
        var _this = this;
        if (this.nome == undefined || this.email == undefined || this.senha == undefined || this.categoria == undefined || this.prova == undefined) {
            this.urlService.alertas('Atenção', 'Preencha todos os campos!');
        }
        else {
            this.postData(this.postagem.value)
                .subscribe(function (suc) {
                console.log(_this.postagem.value);
                console.log("Inserido com sucesso!!!");
                _this.urlService.alertas('Atenção', 'Cadastro realizado com sucesso!');
                _this.navController.navigateBack('');
            }, function (err) {
                console.log(_this.postagem.value);
                console.log("Erro ao tentar inserir!!!");
                _this.urlService.alertas('Atenção', 'Erro ao tentar inserir!!!');
            });
        }
    };
    CadastroPage.prototype.postData = function (tipo) {
        var headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this.Http.post(this.urlService.url + "insertCadastro.php", tipo, {
            headers: headers,
            method: "POST"
        }).pipe(map(function (res) { return res.json(); }));
    };
    CadastroPage.prototype.informacoes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.Http.get(this.urlService.getUrl() + "pegar_provas.php").pipe(map(function (res) {
                    return res.json();
                }))
                    .subscribe(function (data) {
                    _this.provas = (data);
                    console.log(_this.provas);
                });
                return [2 /*return*/];
            });
        });
    };
    CadastroPage.prototype.informacoesCategoria = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.Http.get(this.urlService.getUrl() + "pegar_categoria.php").pipe(map(function (res) {
                    return res.json();
                }))
                    .subscribe(function (data) {
                    _this.categorias = (data);
                    console.log(_this.categorias);
                });
                return [2 /*return*/];
            });
        });
    };
    CadastroPage.prototype.getRetornar = function () {
        var _this = this;
        this.servidor.getPegarCategoria()
            .subscribe(function (data) { return _this.dados = data; }, function (err) { return console.log(err); });
    };
    CadastroPage.prototype.ngOnInit = function () {
    };
    CadastroPage = __decorate([
        Component({
            selector: 'app-cadastro',
            templateUrl: './cadastro.page.html',
            styleUrls: ['./cadastro.page.scss'],
        }),
        __metadata("design:paramtypes", [NavController,
            FormBuilder,
            Http,
            UrlService,
            UsuarioService,
            NavController])
    ], CadastroPage);
    return CadastroPage;
}());
export { CadastroPage };
//# sourceMappingURL=cadastro.page.js.map