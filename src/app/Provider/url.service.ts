import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController, NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  url: string = "http://aquaform.000webhostapp.com/";
  loading = false

  constructor(public navCtrl: NavController ,public alert: AlertController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {

  }

  getUrl() {
    return this.url;
  }

  async alertas(titulo, msg){
    const alert = await this.alert.create({
      header: titulo,
      message: msg,
      buttons: [
        'OK'
      ]
    });
    await alert.present();
  }

  async alertaconfirmSair(titulo, msg){

    const alert = await this.alert.create({
      header: titulo,
      message: msg,
      buttons: [
        {
          text: 'Não',
          role: 'Não',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('CANCELADO');
            
          }
        }, {
          text: 'Sim',
          handler: () => {
            this.limpar();
          }
        }
      ]
    });

    await alert.present();
  }

  limpar(){
    localStorage.clear();
    this.navCtrl.navigateBack('');
    console.log('CONFIRMADO');
  }

  

  async exibirLoading(){
    this.loading = true;
    return await this.loadingCtrl.create({
      message: "Carregando dados..."
    }).then(a => {
      a.present().then(() => {
        if(!this.loading){
          a.dismiss().then(() => {});
        }
      })
    });
  }

  async fecharLoading(){
    this.loading = false;
    return await this.loadingCtrl.dismiss().then(() => {});
  }

  async mostrarToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
