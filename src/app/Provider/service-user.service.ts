import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceUserService {

  userProva = "";
  userTempo = "";
  userMeta = "";
  userCategoria = "";
  userNome = "";
  idUser = "";
  userNivel = "";
  userFoto = "";

  constructor() { }

  setUserNome(valor) {
    this.userNome = valor;
  }

  getUserNome() {
    return localStorage.getItem("userNome");
  }

  setUserCategoria(valor) {
    this.userCategoria = valor;
  }

  getUserCategoria() {
    return localStorage.getItem("userCategoria");
  }

  setUserId(valor) {
    this.idUser = valor;
  }

  getUserId() {
    return this.idUser;
  }

  setUserNivel(valor) {
    this.userNivel = valor;
  }

  getUserNivel() {
    return this.userNivel;
  }

  setUserFoto(valor) {
    this.userFoto = valor;
  }

  getUserFoto() {
    return this.userFoto;
  }

  setUserProva(valor) {
    this.userProva = valor;
  }

  getUserProva() {
    return localStorage.getItem("userProva");
  }

}
