import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  public menu = [
    {
      "label": "Publicar",
      "icone": "appstore",
      "acao": "publicar",
      "menu": "0",
      "exibirMenu": true
    },
    {
      "label": "Cadastro de Usuário",
      "icone": "person-add",
      "acao": "cadastrar_usuario",
      "menu": "1",
      "exibirMenu": true
    },
    {
      "label": "Cadastro de Empresa",
      "icone": "business",
      "acao": "cadastrar_empresa",
      "menu": "2",
      "exibir": true
    },
  ];

  public perfilMenu = [
    
  ];

  constructor() { }
}
