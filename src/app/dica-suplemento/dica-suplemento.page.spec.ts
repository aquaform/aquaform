import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DicaSuplementoPage } from './dica-suplemento.page';

describe('DicaSuplementoPage', () => {
  let component: DicaSuplementoPage;
  let fixture: ComponentFixture<DicaSuplementoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DicaSuplementoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DicaSuplementoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
