import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProvasPage } from './add-provas.page';

describe('AddProvasPage', () => {
  let component: AddProvasPage;
  let fixture: ComponentFixture<AddProvasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProvasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProvasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
