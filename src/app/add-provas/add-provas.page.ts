import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UrlService } from '../Provider/url.service';
import { Http, Headers, Response } from '@angular/http';
import { FormGroup, FormBuilder, FormControl, Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-add-provas',
  templateUrl: './add-provas.page.html',
  styleUrls: ['./add-provas.page.scss'],
})
export class AddProvasPage implements OnInit {

  postagem: any;
  prova: any;
  provas: any;
  dados: any;


  constructor(public navCtrl: NavController,
              public urlService: UrlService,
              public formBuilder: FormBuilder,
              public Http: Http,
              public navController: NavController) {

                this.postagem = this.formBuilder.group({
                  prova: ['', Validators.required]
                });

                this.informacoes();

               }

  ngOnInit() {
  }

  postarProduto(){
    if(this.prova == undefined){
      this.urlService.alertas('Atenção','Preencha todos os campos!');
    } else {
      this.postData(this.postagem.value)
      .subscribe(
        suc => {
          console.log(this.postagem.value);
          console.log("Inserido com sucesso!!!");
          this.urlService.alertas('Atenção','Inserido com sucesso!!!');
          this.navCtrl.navigateBack('tabs/tab4');
        },
        err => {
          console.log(this.postagem.value);
          console.log("Inserido com sucesso!!!");
          this.urlService.alertas('Atenção','Inserido com sucesso!!!');
          this.navCtrl.navigateBack('tabs/tab4');
        }
      )
    }
  }


  async informacoes() {

    this.Http.get(this.urlService.getUrl() + "pegar_provas.php").pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.provas = (data);
              console.log(this.provas);    

        }

      );

  }

  postData(tipo){
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.Http.post(this.urlService.url + "insertProva.php?IDUSUARIO= " + localStorage.getItem('userId'), tipo, {
      headers: headers,
      method: "POST"
    }).pipe(map(
      (res: Response) => {return res.json();}
    ));
  }
}
