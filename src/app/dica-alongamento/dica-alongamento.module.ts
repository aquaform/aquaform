import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DicaAlongamentoPage } from './dica-alongamento.page';

const routes: Routes = [
  {
    path: '',
    component: DicaAlongamentoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DicaAlongamentoPage]
})
export class DicaAlongamentoPageModule {}
