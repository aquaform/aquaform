import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DicaAlongamentoPage } from './dica-alongamento.page';

describe('DicaAlongamentoPage', () => {
  let component: DicaAlongamentoPage;
  let fixture: ComponentFixture<DicaAlongamentoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DicaAlongamentoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DicaAlongamentoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
