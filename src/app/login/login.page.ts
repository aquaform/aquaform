import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { UrlService } from '../../app/Provider/url.service';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { ServiceUserService } from '../../app/Provider/service-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  nome: any;
  email: string;
  senha: string;
  status: string;
  dados: any;

  constructor(public alert: AlertController, public urlService: UrlService, public Http: Http, public navController: NavController, public Loading: LoadingController, public serviceUser: ServiceUserService) { }

  ngOnInit() {
  }

  async logar() {

    if (this.email == undefined || this.senha == undefined) {

      this.urlService.alertas('Atenção', 'Preencha todos os campos!');

    } else {

      const load = await this.Loading.create({
        message: 'Verificando login...',
        spinner: 'circles'
      });
      await load.present();

      this.Http.get(this.urlService.getUrl() + "login.php?email=" + this.email + "&senha=" + this.senha).pipe(map(res =>
        res.json()))
        .subscribe(

          data => {

            if (data.msg.logado == "sim") {
                this.serviceUser.setUserId(data.dados.idusuario);
                this.serviceUser.setUserNome(data.dados.nome);
                localStorage.setItem('userId', data.dados.idusuario);
                localStorage.setItem('userNome', data.dados.nome);
                localStorage.setItem('userCategoria', data.dados.categoria);
                load.dismiss();
                console.log(localStorage.getItem("userNome"));
                this.navController.navigateBack('tabs');

            } else {
              load.dismiss();
              this.urlService.alertas('Atenção', 'Usuário ou senha incorretos!');

            }

          }

        );

    }

  }

}
