import { Component, OnInit } from '@angular/core';
import { UrlService } from '../Provider/url.service';
import { Http, Headers, Response } from '@angular/http';
import { FormBuilder, Validators} from '@angular/forms';
import { NavController } from '@ionic/angular';
import { UsuarioService } from '../usuario.service';
import { map } from "rxjs/operators";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-participa-compe',
  templateUrl: './participa-compe.page.html',
  styleUrls: ['./participa-compe.page.scss'],
})
export class ParticipaCompePage implements OnInit {


  provas: any;
  prova: any;
  postagem: any;
  idcompeticao: any;

  constructor(public navCtrl: NavController,
              public formBuilder: FormBuilder,
              public Http: Http,
              public urlService: UrlService,
              public servidor: UsuarioService,
              public route: ActivatedRoute,
              public navController: NavController) { 

                this.postagem = this.formBuilder.group({
                  prova: ['', Validators.required]
                });

                this.idcompeticao = this.route.snapshot.params['idcompeticao'];
                console.log(this.idcompeticao);

                this.informacoes();
              }

  ngOnInit() {
  }

  cadastrarCompe(){
    if(this.prova == undefined){
      this.urlService.alertas('Atenção','Preencha todos os campos!');
    } else {
      this.postData(this.postagem.value, this.idcompeticao)
      .subscribe(
        suc => {
          console.log(this.postagem.value);
          console.log("Inserido com sucesso!!!");
          this.urlService.alertas('Atenção','Cadastro na competicao realizado com sucesso!');
          this.navController.navigateBack('tabs/tab1');
        },
        err => {
          console.log(this.postagem.value);
          console.log("Erro ao tentar inserir!!!");
          this.urlService.alertas('Atenção','Cadastro na competicao realizado com sucesso!');
        }
      )
    }
  }

  async informacoes() {

    this.Http.get(this.urlService.getUrl() + "pegar_provas.php").pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.provas = (data);
              console.log(this.provas);    

        }

      );

  }

  postData(tipo, idcompeticao: any){
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.Http.post(this.urlService.url + "participaCompe.php?IDCOMPETICAO=" + idcompeticao + "&IDUSUARIO=" + localStorage.getItem('userId'), tipo, {
      headers: headers,
      method: "POST"
    }).pipe(map(
      (res: Response) => {return res.json();}
    ));
  }

}
