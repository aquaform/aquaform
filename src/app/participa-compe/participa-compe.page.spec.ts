import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipaCompePage } from './participa-compe.page';

describe('ParticipaCompePage', () => {
  let component: ParticipaCompePage;
  let fixture: ComponentFixture<ParticipaCompePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipaCompePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipaCompePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
