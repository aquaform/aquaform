import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompeticaoPage } from './competicao.page';

describe('CompeticaoPage', () => {
  let component: CompeticaoPage;
  let fixture: ComponentFixture<CompeticaoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompeticaoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompeticaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
