import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-competicao',
  templateUrl: './competicao.page.html',
  styleUrls: ['./competicao.page.scss'],
})
export class CompeticaoPage implements OnInit {

  idcompeticao: any;
  dados:any;

  dadoItem: Array <{nome:any}>
  constructor(public servidor: UsuarioService,
              public navCtrl: NavController) {

    this.getRetornar();
   }

  ngOnInit() {
  }

  participar(idcompeticao:any) {
    this.navCtrl.navigateBack('participa-compe/' + idcompeticao);
    //this.router.navigate(['add-tempos'], idprova);
  }
  
  getRetornar(){

    this.servidor.getCompeticao()
    .subscribe(
      data => this.dados = data,
      err => console.log(err)
    );

    this.servidor.getCidade()
    .subscribe(
      data => this.dadoItem = data,
      err => console.log(err)
    );

  }

  

}
