import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../Provider/url.service";
import { NavController } from '@ionic/angular';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  postagem: any;
  nome: any;
  //ano: any;
  email: any;
  senha: any;
  prova: any;
  provas: any;
  categoria: any;
  categorias: any;
  dados: any;

  constructor(public navCtrl: NavController,
              public formBuilder: FormBuilder,
              public Http: Http,
              public urlService: UrlService,
              public servidor: UsuarioService,
              public navController: NavController
              ) { 
                this.postagem = this.formBuilder.group({
                  nome: ['', Validators.required],
                  categoria: ['', Validators.required],
                  //ano: ['', Validators.required],
                  email: ['', Validators.required], 
                  senha: ['', Validators.required],
                  prova: ['', Validators.required]
                });

                this.getRetornar();
                this.informacoes();
                this.informacoesCategoria();
            
              }

  postarProduto(){
    if(this.nome == undefined || this.email == undefined || this.senha == undefined || this.categoria == undefined || this.prova == undefined){
      this.urlService.alertas('Atenção','Preencha todos os campos!');
    } else {
      this.postData(this.postagem.value)
      .subscribe(
        suc => {
          console.log(this.postagem.value);
          console.log("Inserido com sucesso!!!");
          this.urlService.alertas('Atenção','Cadastro realizado com sucesso!');
          this.navController.navigateBack('');
        },
        err => {
          console.log(this.postagem.value);
          console.log("Erro ao tentar inserir!!!");
          this.urlService.alertas('Atenção','Erro ao tentar inserir!!!');
        }
      )
    }
  }

  postData(tipo){
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.Http.post(this.urlService.url + "insertCadastro.php", tipo, {
      headers: headers,
      method: "POST"
    }).pipe(map(
      (res: Response) => {return res.json();}
    ));
  }

  async informacoes() {

    this.Http.get(this.urlService.getUrl() + "pegar_provas.php").pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.provas = (data);
              console.log(this.provas);    

        }

      );

  }

  async informacoesCategoria() {

    this.Http.get(this.urlService.getUrl() + "pegar_categoria.php").pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.categorias = (data);
              console.log(this.categorias);    

        }

      );

  }

  getRetornar(){

    this.servidor.getPegarCategoria()
    .subscribe(
      data => this.dados = data,
      err => console.log(err)
    );

  }

  ngOnInit() {
  }

}
