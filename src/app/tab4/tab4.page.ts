import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { UsuarioService } from '../usuario.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UrlService } from '../../app/Provider/url.service';
import { Http } from '@angular/http';
import { ServiceUserService } from '../../app/Provider/service-user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  competicoes: any;
  competicao: any;
  dados: any;
  idusuario: any;
  provas: any;

  constructor(public navCtrl: NavController,  
              public servidor: UsuarioService, 
              public http: HttpClient, 
              public serviceUser: ServiceUserService,
              public Http : Http,
              public urlService: UrlService, 
              public Loading: LoadingController,
              public router: Router,
              public alert: AlertController) { 

    this.getRetornar();
    this.getRetornarCompeticoes();
    this.informacoes();
    this.informacoesCompeticoes();
    this.informacoesUsuario();

  }


  ngOnInit() {
  }

  alterar(idprova:any) {
    this.navCtrl.navigateBack('add-tempos/' + idprova);
    //this.router.navigate(['add-tempos'], idprova);
  }



  async Botaoexcluir(idprova:any) {

    this.alertaconfirmExluir('Atenção','Deseja mesmo excluir?',idprova)
    
  }

  async informacoesUsuario() {

    this.Http.get(this.urlService.getUrl() + "dados_usuario.php?IDUSUARIO= " + localStorage.getItem('userId')).pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.provas = (data);
              console.log(this.provas);    

        }

      );

  }


  async informacoes() {

      this.Http.get(this.urlService.getUrl() + "dados_provas.php?IDUSUARIO= " + localStorage.getItem('userId')).pipe(map(res =>
        res.json()))
        .subscribe(

          data => {
        
                this.provas = (data);
                console.log(this.provas);
                //this.navCtrl.navigateBack('tabs');     

          }

        );

    }

    async informacoesCompeticoes() {

      this.Http.get(this.urlService.getUrl() + "pegar_competicoes.php?IDUSUARIO= " + localStorage.getItem('userId')).pipe(map(res =>
        res.json()))
        .subscribe(

          data => {
        
                this.competicao = (data);
                console.log(this.competicao);
                //this.navCtrl.navigateBack('tabs');     

          }

        );

    }

    getRetornar(){

      this.servidor.getPegar()
      .subscribe(
        data => this.dados = data,
        err => console.log(err)
      );

    }

    getRetornarCompeticoes(){

      this.servidor.getPegarCompeticoes()
      .subscribe(
        data => this.competicoes = data,
        err => console.log(err)
      );

    }


    botaoSair(){
    
      this.urlService.alertaconfirmSair('Atenção','Tem certeza que deseja sair?'); 
      
    }

    async alertaconfirmExluir(titulo, msg, idprova){

      const alert = await this.alert.create({
        header: titulo,
        message: msg,
        buttons: [
          {
            text: 'Não',
            role: 'Não',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('CANCELADO');
              
            }
          }, {
            text: 'Sim',
            handler: () => {
              this.Excluir(idprova);
            }
          }
        ]
      });
  
      await alert.present();
    }
  
    Excluir(idprova:any){
      this.Http.get(this.urlService.getUrl() + "DeleteTempo.php?IDUSUARIO= " + localStorage.getItem('userId') + "&IDPROVA=" + idprova).pipe(map(res =>
        res.json()))
        .subscribe(
  
          data => {
        
                this.provas = (data);
                console.log(this.provas);
                //this.navCtrl.navigateBack('tabs');     
  
          }
  
        );
      console.log('CONFIRMADO');
      location.reload();
      //this.router.navigate(['tab4']);
      //this.navCtrl.navigateBack('tab4');
    }

  }

  


