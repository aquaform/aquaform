import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DicaTreinoPage } from './dica-treino.page';

const routes: Routes = [
  {
    path: '',
    component: DicaTreinoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DicaTreinoPage]
})
export class DicaTreinoPageModule {}
