import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DicaTreinoPage } from './dica-treino.page';

describe('DicaTreinoPage', () => {
  let component: DicaTreinoPage;
  let fixture: ComponentFixture<DicaTreinoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DicaTreinoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DicaTreinoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
