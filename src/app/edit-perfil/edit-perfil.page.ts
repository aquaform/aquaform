import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Http, Headers, Response } from '@angular/http';
import { UsuarioService } from '../usuario.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UrlService } from '../Provider/url.service';
import { FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-edit-perfil',
  templateUrl: './edit-perfil.page.html',
  styleUrls: ['./edit-perfil.page.scss'],
})
export class EditPerfilPage implements OnInit {

  novonome: any;
  categoria: any;
  categorias: any;
  provas: any;
  postagem: any;
  dados: any;
  contatos: any;
  url: string = "http://localhost/php/";

  constructor(public navCtrl: NavController,  
              public servidor: UsuarioService,
              public http: HttpClient,
              public formBuilder: FormBuilder,
              public Http: Http,
              public urlService: UrlService,) { 

                this.postagem = this.formBuilder.group({
                  novonome: ['', Validators.required],
                  categoria: ['', Validators.required]
                });

      this.informacoes();
      this.getRetornar();
      this.informacoesCategoria();
  }

  alterPerfil(){
    if(this.novonome == undefined || this.categoria == undefined){
      this.urlService.alertas('Atenção','Preencha todos os campos!');
    } else {
      this.postData(this.postagem.value)
      .subscribe(
        suc => {
          console.log(this.postagem.value);
          console.log("Inserido com sucesso!!!");
          this.urlService.alertas('Atenção','Dados alterados com sucesso!');
          this.navCtrl.navigateBack('tabs/tab4');
        },
        err => {
          console.log(this.postagem.value);
          //console.log("Erro ao tentar inserir!!!");
          this.urlService.alertas('Atenção','Dados inseridos com sucesso!!!');
          this.navCtrl.navigateBack('tabs/tab4');
        }
      )
    }
  }

  postData(tipo){
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.Http.post(this.urlService.url + "alterPerfil.php?IDUSUARIO=" + localStorage.getItem('userId'), tipo, {
      headers: headers,
      method: "POST"
    }).pipe(map(
      (res: Response) => {return res.json();}
    ));
  }

  async informacoes() {

    this.Http.get(this.urlService.getUrl() + "dados_usuario.php?IDUSUARIO= " + localStorage.getItem('userId')).pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.provas = (data);
              console.log(this.provas);    

        }

      );

  }

  async informacoesCategoria() {

    this.Http.get(this.urlService.getUrl() + "pegar_categoria.php").pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.categorias = (data);
              console.log(this.categorias);    

        }

      );

  }

  getRetornar(){

    this.servidor.getPegarCategoria()
    .subscribe(
      data => this.dados = data,
      err => console.log(err)
    );

  }


  ngOnInit() {
  }

  

}
