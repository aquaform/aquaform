import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTemposPage } from './add-tempos.page';

describe('AddTemposPage', () => {
  let component: AddTemposPage;
  let fixture: ComponentFixture<AddTemposPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTemposPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTemposPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
