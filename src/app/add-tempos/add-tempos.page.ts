import { Component, OnInit } from '@angular/core';
import { ServiceUserService } from '../Provider/service-user.service';
import { Http, Headers, Response } from '@angular/http';
import { UrlService } from '../Provider/url.service';
import { LoadingController, NavController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { UsuarioService } from '../usuario.service';
import { Router, ActivatedRoute } from '@angular/router';
import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-tempos',
  templateUrl: './add-tempos.page.html',
  styleUrls: ['./add-tempos.page.scss'],
})
export class AddTemposPage implements OnInit {

  postagem: any;
  minutos: any =  [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35];
  segundos: any =  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59];
  decimos: any =  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99];
  minu: any;
  segu: any;
  deci: any;
  metaminu: any;
  metasegu: any;
  metadeci: any;
  competicoes: any;
  competicao: any;
  dados: any = [];
  idusuario: any;
  provas: any;
  idprova: any;

  constructor( public serviceUser: ServiceUserService,
               public servidor: UsuarioService,
               public formBuilder: FormBuilder,
               public Http : Http,
               public urlService: UrlService, 
               public Loading: LoadingController,
               public navCtrl: NavController,
               public route: ActivatedRoute) { 

              this.postagem = this.formBuilder.group({
                minu: ['', Validators.required],
                segu: ['', Validators.required],
                deci: ['', Validators.required],
                metaminu: ['', Validators.required],
                metasegu: ['', Validators.required],
                metadeci: ['', Validators.required]
              });

                this.idprova = this.route.snapshot.params['idprova'];
                console.log(this.idprova);
              
                
                this.getRetornar();
                this.informacoes();

               }

  ngOnInit() {
  }

  insertTempo(){
    if(this.segu == undefined || this.deci == undefined || this.metasegu == undefined || this.metadeci == undefined){
      this.urlService.alertas('Atenção','Preencha todos os campos!');
    } else {
      this.postData(this.postagem.value)
      .subscribe(
        suc => {
          console.log(this.postagem.value);
          console.log("Inserido com sucesso!!!");
          this.urlService.alertas('Atenção','Cadastro realizado com sucesso!');
          this.navCtrl.navigateBack('tabs/tab4');
        },
        err => {
          console.log(this.postagem.value);
          //console.log("Erro ao tentar inserir!!!");
          this.urlService.alertas('Atenção','Dados inseridos com sucesso!!!');
          this.navCtrl.navigateBack('tabs/tab4');
        }
      )
    }
  }

  excluirTempo(){

  }

  postData(tipo){
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.Http.post(this.urlService.url + "insertTempo.php?IDUSUARIO=" + localStorage.getItem('userId') + "&IDPROVA=" + this.idprova, tipo, {
      headers: headers,
      method: "POST"
    }).pipe(map(
      (res: Response) => {return res.json();}
    ));
  }

  async informacoes() {

    this.Http.get(this.urlService.getUrl() + "dados_alterar_provas.php?IDPROVA=" + this.idprova).pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.provas = (data);
              console.log(this.provas);
              //this.navCtrl.navigateBack('tabs');     

        }

      );

  }

  getRetornar(){

    this.servidor.getPegarAlterarProva(this.idprova)
    .subscribe(
      data => this.dados = data,
      err => console.log(err)
    );

  }

}
