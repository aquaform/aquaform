import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DicaMusculacaoPage } from './dica-musculacao.page';

const routes: Routes = [
  {
    path: '',
    component: DicaMusculacaoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DicaMusculacaoPage]
})
export class DicaMusculacaoPageModule {}
