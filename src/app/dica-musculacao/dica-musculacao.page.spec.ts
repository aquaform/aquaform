import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DicaMusculacaoPage } from './dica-musculacao.page';

describe('DicaMusculacaoPage', () => {
  let component: DicaMusculacaoPage;
  let fixture: ComponentFixture<DicaMusculacaoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DicaMusculacaoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DicaMusculacaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
