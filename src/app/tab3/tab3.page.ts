import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../Provider/url.service";
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  postagem: any;
  inicio: any;
  horario: any;
  tipo: any;
  descricao: any;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public http: Http,
    public urlService: UrlService,
  )
  {
    this.postagem = this.formBuilder.group({
      inicio: ['', Validators.required],
      horario: ['', Validators.required], 
      tipo: ['', Validators.required],
      descricao: ['', Validators.required]
    });
  }

  postarProduto(){
    if(this.inicio == undefined || this.horario == undefined || this.tipo == undefined || this.descricao == undefined){
      this.urlService.alertas('Atenção','Preencha todos os campos!');
    } else {
      this.postData(this.postagem.value)
      .subscribe(
        data => {
          console.log("Inserido com sucesso!!!");
          this.urlService.alertas('Atenção','Inserido com sucesso!!!');
          // this.navCtrl.navigateBack('tabs/tab1' + location.reload() );
          this.navCtrl.navigateBack('tabs/tab1'  + location.reload());
          location.reload();
        },
        err => {
          console.log("Erro ao tentar inserir!!!");
          this.urlService.alertas('Atenção','Inserido com sucesso!!!');
          // this.navCtrl.navigateBack('tabs/tab1/' + location.reload() );
          this.navCtrl.navigateBack('tabs/tab1' + location.reload());
          // location.reload();
          
        }
      )
    }
  }

  postData(tipo){
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.http.post(this.urlService.url + "insertEvento.php?IDUSUARIO=" + localStorage.getItem('userId'), tipo, {
      headers: headers,
      method: "POST"
    }).pipe(map(
      (res: Response) => {return res.json();}
    ));
  }

  ngOnInit(){

  }
}
