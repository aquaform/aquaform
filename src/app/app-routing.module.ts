import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'tab4', loadChildren: './tab4/tab4.module#Tab4PageModule' },
  { path: 'dica-alongamento', loadChildren: './dica-alongamento/dica-alongamento.module#DicaAlongamentoPageModule' },
  { path: 'dica-treino', loadChildren: './dica-treino/dica-treino.module#DicaTreinoPageModule' },
  { path: 'dica-musculacao', loadChildren: './dica-musculacao/dica-musculacao.module#DicaMusculacaoPageModule' },
  { path: 'dica-suplemento', loadChildren: './dica-suplemento/dica-suplemento.module#DicaSuplementoPageModule' },
  { path: 'agenda', loadChildren: './agenda/agenda.module#AgendaPageModule' },
  { path: 'competicao', loadChildren: './competicao/competicao.module#CompeticaoPageModule' },
  { path: 'agenda', loadChildren: './agenda/agenda.module#AgendaPageModule' },
  { path: 'edit-perfil', loadChildren: './edit-perfil/edit-perfil.module#EditPerfilPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'cadastro', loadChildren: './cadastro/cadastro.module#CadastroPageModule' },
  { path: 'add-tempos/:idprova', loadChildren: './add-tempos/add-tempos.module#AddTemposPageModule' },
  { path: 'add-provas', loadChildren: './add-provas/add-provas.module#AddProvasPageModule' },
  { path: 'participa-compe/:idcompeticao', loadChildren: './participa-compe/participa-compe.module#ParticipaCompePageModule' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
