import { Component } from '@angular/core';
import { NgModule, ViewChild } from '@angular/core';
import { IonicModule, NavController } from '@ionic/angular';
import { map } from "rxjs/operators";
import chartJs from 'chart.js';
import { UsuarioService } from '../usuario.service';
import { UrlService } from '../Provider/url.service';
import { Http } from '@angular/http';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  @ViewChild('barCanvas') barCanvas;

  dados: any;
  grafico: any;
  barChart: any;

  constructor(public navCtrl: NavController,
              public urlService: UrlService,   
              public Http: Http,
              public servidor: UsuarioService) {

    this.getRetornar();
  }

  ngAfterViewInit(){
    this.dadosgrafico();
    setTimeout(() => {
        this.barChart = this.getBarChart();
    }, 1500)
  }

  getChart(context, chartType, data, options?){
    return new chartJs(context, {
      data,
      options,
       type: chartType
    })
  }

  async dadosgrafico() {

    this.Http.get(this.urlService.getUrl() + "pegar_dados_grafico.php?IDUSUARIO=" + localStorage.getItem('userId')).pipe(map(res =>
      res.json()))
      .subscribe(

        data => {
      
              this.grafico = data;
              console.log(this.grafico);    

        }

      );

  }

  async getBarChart(){
    
    const data = {
      labels: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      datasets: [{
        label: 'Compromissos da semana',
        data: this.grafico,
        backgroundColor: [
          'rgb(56, 128, 255)',
          'rgb(56, 128, 255)',
          'rgb(56, 128, 255)',
          'rgb(56, 128, 255)',
          'rgb(56, 128, 255)',
          'rgb(56, 128, 255)',
          'rgb(56, 128, 255)'
        ],
        borderWidth: 1
      }]
    };

    const options = {
      scales: {
        yAxes: [{
          ticks:{
            beginAtZero: true,
          }
        }]
      }
    }
    return this.getChart(this.barCanvas.nativeElement, 'bar', data, options)
  }

  getRetornar(){

    this.servidor.getCompeticao()
    .subscribe(
      data => this.dados = data,
      err => console.log(err)
    );

  }
  
}
