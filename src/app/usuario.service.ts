import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url: string = "http://aquaform.000webhostapp.com/";

  constructor(public http: Http, public alert: AlertController) {
    console.log('ttello ServidorProvider Provider');
  }

  getPegarProva() {
    return this.http.get(this.url + "dados_provas.php").pipe(map(res => res.json()));
  }

  getPegarCategoria() {
    return this.http.get(this.url + "pegar_categoria.php").pipe(map(res => res.json()));
  }
  
  getPegar() {
    return this.http.get(this.url + "dados_provas.php?IDUSUARIO= " + localStorage.getItem('userId')).pipe(map(res => res.json()));
  }

  getPegarAlterarProva(idprova: any) {
    return this.http.get(this.url + "dados_alterar_provas.php?IDPROVA= " + idprova).pipe(map(res => res.json()));
  }

  getPegarCompeticoes() {
    return this.http.get(this.url + "pegar_competicoes.php?IDUSUARIO= " + localStorage.getItem('userId')).pipe(map(res => res.json()));
  }

  getCompeticao() {
    return this.http.get(this.url + "dados_competicao.php").pipe(map(res => res.json()));
  }

  getCidade() {
    return this.http.get(this.url + "dados_cidade.php").pipe(map(res => res.json()));
  }

  async alertas(titulo, msg) {
    const alert = await this.alert.create({
      header: titulo,
      message: msg,
      buttons: [
        'OK'
      ]
    });
    await alert.present();
  }
}
